# The n-Queen Puzzle
In this project I made a program that receives an integer, called n, in the range [1, 20], and prints out one solution (if any exist) to the problem of n-queen. (In this problem, n queens will be put on an n*n chess board such that no queen can attack any other.)

#
## Using the program
To use the program you simply types in the wanted width of the game board (n * n) and see the result get printed to the terminal window.

As of this point in time, the program can calculate up to 20 before it begins to use more than 10 seconds to find an solution.

## How it solves the problem
The way it solves the board is by brute forcing its way to the first solution. I generates the first queen in position 0,0 and then uses the validation and trial and error to find the next possiple location for the second queen using a factorial function. After the program has ran through the whole board sucssessfully it will then print it to the terminal in 1s and 0s where a queen is 1 and 0 is blank space.

In the validation prosess it lookes at the placement for the current queen and sees if it alignes with other queens.

## Creator of this project
Alexander Øvergård