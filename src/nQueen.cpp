// inital setup
#include <iostream>
#include <vector>

using Vector = std::vector<std::vector<int>>; // to shorten code inside functions

int size;

// generates a blank game board to later fill inn
Vector boardGenerator(int width)
{
	Vector gameBoard; // vector in vector

	// populate the vector with 0
	for (int i = 0; i < width; i++)
	{
		std::vector<int> x(width, 0);
		gameBoard.push_back(x);
	}

	return gameBoard;
}

// prints out the gameboard to the terminal window
void printBoard(Vector gameBoard)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			std::cout << gameBoard[j][i] << "  ";
		}
		std::cout << std::endl;
	}
}

bool valid(Vector gameBoard, int y, int x)
{
	// counters used to count queens
	int row = 0;
	int col = 0;
	int upRight = 0;
	int upLeft = 0;

	// check collomn
	for (int i = 0; i < gameBoard.size(); i++)
	{
		col += gameBoard[i][x];
		if (col > 1)
			return false;
	}

	// check row
	for (int i = 0; i < gameBoard.size(); i++)
	{
		row += gameBoard[y][i];
		if (row > 1)
			return false;
	}

	// Check up right
	int xpos = x;
	int ypos = y;
	while (ypos >= 0 && xpos < gameBoard.size())
	{
		upRight += gameBoard[ypos][xpos];
		xpos += 1;
		ypos -= 1;

		if (upRight > 1)
			return false;
	}

	// check up left
	xpos = x;
	ypos = y;
	while (ypos >= 0 && xpos >= 0)
	{
		upLeft += gameBoard[ypos][xpos];
		xpos -= 1;
		ypos -= 1;

		if (upLeft > 1)
			return false;
	}

	return true;
}

// recursion to find if placed queen is indeed correct
bool populateBoard(int y, Vector &gameBoard)
{
	if (y >= gameBoard.size())
	{
		return true;
	}

	for (int x = 0; x < gameBoard.size(); x++)
	{
		gameBoard[y][x] = 1;
		if (valid(gameBoard, y, x))
		{
			bool finished = populateBoard(y + 1, gameBoard);
			if (finished)
			{
				return true;
			}
		}
		gameBoard[y][x] = 0;
	}
	return false;
}

int main()
{
	// input for the user to choose the board size
	std::cout << "Input the size of board: ";
	std::cin >> size;

	Vector gameBoard = boardGenerator(size); // generates a n*n board of "0" to fill later

	// adds a "1" to the board then checking if the action is allowed repeatedly until the board is full
	bool ready = populateBoard(0, gameBoard);

	// if it finds a solution it will promt it to print the board to terminal
	if (ready)
		printBoard(gameBoard);
}